<?php 

return [
    'global' => [
        "server_key" => env('OTTOPAY_SERVER_KEY'),
        "merchant" => env('OTTOPAY_MERCHANT_ID'),
    ],
    'warung' => [
        "server_key" => env('OTTOPAY_WARUNG_SERVER'),
        "merchant" => env('OTTOPAY_WARUNG_MERCHANT'),
    ],
    'kimonu' => [
        "server_key" => env('OTTOPAY_KIMONU_SERVER'),
        "merchant" => env('OTTOPAY_KIMONU_MERCHANT'),
    ]

];

?>