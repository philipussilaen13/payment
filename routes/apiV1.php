<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes V1
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "apiV1" middleware group. Enjoy building your API!
|
*/

Route::post('testing',function(){
    return 'testing v1';
});

Route::post('createSession','ApiController@createSession');

Route::prefix('payment')->group(function(){
   Route::post('getPaymentMethod','PaymentController@getPaymentMethod');
   Route::post('createPayment','PaymentController@createPayment');
   Route::post('inquiryPayment','PaymentController@inquiryPayment');
   Route::post('paidPayment','PaymentController@paidPayment');
   Route::post('pushRetry','PaymentController@retryPushCallback');
});