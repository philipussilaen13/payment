<?php
/**
 * Created by PhpStorm.
 * User: arief
 * Date: 23/08/2017
 * Time: 09.58
 */

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::get('doku/{paymentHashed}','WebViewController@dokuHosted');
Route::get('/tcash/{tokenPgp}', function ($tokenPgp) {
    return view('tcash.tcash_web')->with('tokenPgp', $tokenPgp);
});