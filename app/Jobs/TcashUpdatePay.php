<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Http\Models\apiV1\ClientTransaction;
use App\Http\Models\apiV1\ClientTransactionDetail;
use App\Http\Models\apiV1\ClientTransactionHistory;
use App\Http\Models\apiV1\TcashTransactions;
use App\Http\Models\apiV1\CheckUpdateTemp;
use Illuminate\Support\Facades\DB;
use App\Http\Helper\Helper;

class TcashUpdatePay implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $checkUpdateTemp;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($checkUpdateTemp)
    {
        $this->checkUpdateTemp = $checkUpdateTemp;        
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $checkUpdateTrx      = $this->checkUpdateTemp;
        $clientTransactionId = $checkUpdateTrx->order_id; 
        $statusPay   = ($checkUpdateTrx->transaction_status == 'FAILED' || $checkUpdateTrx->transaction_status == 'EXPIRED') ? 'FAILED' : 'PAID';
        $statusTrx   = 'success';

        if ($checkUpdateTrx->transaction_status == 'FAILED') {
            $statusTrx = 'failed';
        } elseif ($checkUpdateTrx->transaction_status == 'EXPIRED') {
            $statusTrx = 'expired';
        }

        $tcashDb = TcashTransactions::with('clientTransaction')
                        ->where('client_transactions_id', $clientTransactionId)
                        ->first();

        $paymentId = $tcashDb->clientTransaction->payment_id;
        
        // Begin Transaction
        DB::beginTransaction();        
    
        $paymentAmount = isset($checkUpdateTrx->gross_amount) ? $checkUpdateTrx->gross_amount : null;
        $clientTransactionDb = ClientTransaction::where('payment_id', $paymentId)->first();
        $clientTransactionId = $clientTransactionDb->id;
        $clientTransactionDb->status = $statusPay;
        $clientTransactionDb->total_payment = $paymentAmount;
        $clientTransactionDb->last_payment = $paymentAmount;
        $clientTransactionDb->save();

        $tcashDb = TcashTransactions::where('client_transactions_id', $clientTransactionDb->id)->first();
        $tcashDb->last_payment_amount = isset($checkUpdateTrx->gross_amount) ? $checkUpdateTrx->gross_amount : null;
        $tcashDb->param_payment      = json_encode( $checkUpdateTrx->param_req );
        $tcashDb->payment_date        = isset($checkUpdateTrx->transaction_time) ? $checkUpdateTrx->transaction_time : null;
        $tcashDb->status              = $statusTrx;
        $tcashDb->save();

        // update transactionDB
        Helper::LogPayment("Update Transaction $clientTransactionId to $statusPay",'tcash','inquiry');

        // insert history
        ClientTransactionHistory::insertHistory($clientTransactionId,$statusPay,$statusPay,$paymentAmount);
        Helper::LogPayment("Insert History Transaction $statusPay",'tcash','inquiry');

        // update client transaction detail to paid
        $clientTransactionDetailDb = ClientTransactionDetail::where('client_transactions_id',$clientTransactionId)->first();
        if ($clientTransactionDetailDb){
            $clientTransactionDetailDb = ClientTransactionDetail::find($clientTransactionDetailDb->id);
            $clientTransactionDetailDb->paid_amount = $paymentAmount;
            $clientTransactionDetailDb->status = $statusPay;
            $clientTransactionDetailDb->save();
        }

        if (!$clientTransactionDb->save() || !$tcashDb->save() || !$clientTransactionDetailDb->save()) {
            DB::rollBack();
            CheckUpdateTemp::where('order_id', $checkUpdateTrx->order_id)->delete();
            Helper::LogPayment("Gagal Melakukan Transaction dengan payment_id ". $clientTransactionDb->payment_id ,'tcash','inquiry');
            echo "Gagal Melakukan Transaction dengan payment_id ". $clientTransactionDb->payment_id;
        }

        DB::commit();
        CheckUpdateTemp::where('order_id', $checkUpdateTrx->order_id)->delete();
        Helper::LogPayment("Berhasil Melakukan Transaction dengan payment_id ". $clientTransactionDb->payment_id ,'tcash','inquiry');
        echo "Transaction Berhasil \n";
    }
}
