<?php

namespace App\Http\Controllers\apiV1;

use App\Http\Helper\apiV1\ApiHelper;
use App\Http\Helper\Helper;
use App\Http\Models\apiV1\ApiSession;
use App\Http\Models\apiV1\Company;
use App\Http\Models\apiV1\CompanyAccessToken;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

/**
 * Class ApiController
 * @package App\Http\Controllers\apiV1
 */
class ApiController extends Controller
{
    /**
     * Create Session for Access API
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createSession(Request $request){
        $status = 400;
        $message = null;

        //required param list
        $required = ['client_id'];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)){
            $message = "Missing Parameter : ".implode(', ',$paramFailed);
            return ApiHelper::buildResponse($status,$message);
        }

        $clientId = $request->input('client_id');
        $token = $request->input('token');
        $ipAddress = $request->ip();
        // check token and client id
        $companyDb = Company::where('client_id',$clientId)->first();
        if (!$companyDb){
            $message = 'Invalid Client ID';
            return ApiHelper::buildResponse($status,$message);
        }
        $companyAccessTokenDb = CompanyAccessToken::where('token',$token)
            ->where('companies_id',$companyDb->id)
            ->first();
        if (!$companyAccessTokenDb){
            $message = 'Invalid Client ID and Token';
            return ApiHelper::buildResponse($status,$message);
        }

        DB::beginTransaction();
        // generate session Id
        $apiSessionDB = ApiSession::createSession($companyAccessTokenDb->id,$ipAddress);
        if (!$apiSessionDB->isSuccess){
            DB::rollback();
            $message = 'Failed to Create Session';
            return ApiHelper::buildResponse($status,$message);
        }
        DB::commit();
        $sessionId = $apiSessionDB->sessionId;
        // set session id
        session(['session_id'=>$sessionId]);
        // create response
        $response = new \stdClass();
        $response->session_id = $sessionId;
        $status = 200;

        return ApiHelper::buildResponse($status,$message,$response);
    }
}
