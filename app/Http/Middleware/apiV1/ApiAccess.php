<?php

namespace App\Http\Middleware\apiV1;

use Closure;

class ApiAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // set start time
        $startTime = microtime(true);
        // get token
        $token = $request->input('token',null);
        $sessionId = $request->input('session_id',null);
        // get url
        $url = $request->path();
        // get IP
        $ip = $request->ip();
        //get param
        $param = json_encode($request->except('token'));
        $this->log("Token:$token Session:$sessionId - $ip - $url - $param");

        $response = $next($request);
        // calculate duration
        $endTime = microtime(true);
        $duration = ($endTime - $startTime);

        $logResponse = ($response->content());
        $this->log("Token:$token Session:$sessionId $duration $logResponse");
        return $response;
    }

    private function log($msg=''){
        $msg = " $msg\n";
        $f = fopen(storage_path().'/logs/access/'.date('Y.m.d.').'log','a');
        fwrite($f,date('H:i:s')." $msg");
        fclose($f);
    }
}
