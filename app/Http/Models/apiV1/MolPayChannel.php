<?php

namespace App\Http\Models\apiV1;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MolPayChannel extends Model
{
    protected $table = 'molpay_channel_lists';
    use SoftDeletes;
}
