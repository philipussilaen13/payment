<?php

namespace App\Http\Models\apiV1;

use Illuminate\Database\Eloquent\Model;

class BNIPrefixToken extends Model
{
    protected $table = 'bni_virtual_prefix_token';
}
