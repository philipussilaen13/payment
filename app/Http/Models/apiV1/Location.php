<?php

namespace App\Http\Models\apiV1;

use App\Http\Helper\Helper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Location extends Model
{
    protected $table = 'locations';

    public static function createLocation(Request $request){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->locationId = null;

        $locationType = $request->input('location_type',null);
        $locationName = $request->input('location_name',null);
        $locationAddress = $request->input('location_address',null);

        Helper::LogPayment("Create Location $locationName $locationType");
        if (empty($locationType) || empty($locationName)){
            // for temporary get true, because not mandatory for location
            $response->isSuccess = true;
            return $response;
        }

        // check on Databases
        Helper::LogPayment("Check Location $locationName $locationType");
        $checkDb = Location::where('location_type',$locationType)
            ->where('location_name',$locationName)
            ->first();
        if ($checkDb){
            Helper::LogPayment("Location Exist, ID $checkDb->id");
            $response->isSuccess = true;
            $response->locationId = $checkDb->id;
            return $response;
        }

        Helper::LogPayment('Insert to Table Locations');

        // generate location ID
        $locationId = $locationType.Helper::generateRandomString(4);

        $locationDb = new Location();
        $locationDb->location_id = $locationId;
        $locationDb->location_type = $locationType;
        $locationDb->location_name = $locationName;
        $locationDb->address = $locationAddress;
        $locationDb->save();

        $response->isSuccess = true;
        $response->locationId = $locationDb->id;

        Helper::LogPayment('Finish insert to Table Locations');
        return $response;
    }
}
