<?php

namespace App\Http\Models\apiV1;

use App\Http\Helper\Helper;
use App\Http\Libraries\BNIHashing;
use Illuminate\Database\Eloquent\Model;

class BNIVirtualAccount extends Model
{
    protected $table = 'bni_virtual_accounts';

    /**
     * Create Virtual Account BNI
     * @param $clientTransactionId
     * @param string $billingType
     * @param string $virtualAccount
     * @param $datetimeExpired
     * @return \stdClass
     */
    public static function createVA($clientTransactionId,$billingType='c',$virtualAccount='',$datetimeExpired){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->BNIVaId = null;
        Helper::LogPayment("Begin Create VA",'bni');
        // get transaction
        $clientTransactionDb = ClientTransaction::find($clientTransactionId);
        if (!$clientTransactionDb){
            $response->errorMsg = 'Invalid Transaction';
            return $response;
        }

        $paymentId = $clientTransactionDb->payment_id;
        $transactionAmount = $clientTransactionDb->total_amount;
        $customerName = $clientTransactionDb->customer_name;
        $customerPhone = $clientTransactionDb->customer_phone;
        $customerEmail = $clientTransactionDb->customer_email;
        $description = $clientTransactionDb->description;

        if ($billingType=='o'){
            $transactionAmount = 0;
        }

        $param = [];
        $param['type'] = 'createbilling';
        $param['trx_id'] = $paymentId;
        $param['trx_amount'] = $transactionAmount;
        $param['billing_type'] = $billingType;
        $param['customer_name'] = $customerName;
        $param['customer_email'] = $customerEmail;
        $param['customer_phone'] = $customerPhone;
        $param['virtual_account'] = $virtualAccount;
        $param['datetime_expired'] = date('c', strtotime($datetimeExpired));
        $param['description'] = $description;
        Helper::LogPayment("Create Parameter ".json_encode($param),'bni');

        // push to BNI
        $apiBNI = new \App\Http\Libraries\BNIVirtualAccount();
        $apiResponse = $apiBNI->postVA($param);
        if (!$apiResponse->isSuccess){
            Helper::LogPayment("Failed ".$apiResponse->errorMsg,'bni');
            $response->errorMsg = $apiResponse->errorMsg;
            return $response;
        }
        $apiResponseData = $apiResponse->data;
        $virtualAccount = $apiResponseData['virtual_account'];

        // save to Db
        $BNIDb = new self();
        $BNIDb->client_transactions_id = $clientTransactionId;
        $BNIDb->billing_type = $billingType;
        $BNIDb->virtual_account_number = $virtualAccount;
        $BNIDb->datetime_created = date('Y-m-d H:i:s');
        $BNIDb->datetime_expired = date('Y-m-d H:i:s', strtotime($datetimeExpired));
        $BNIDb->datetime_payment = null;
        $BNIDb->transaction_amount = $transactionAmount;
        $BNIDb->payment_ntb = null;
        $BNIDb->va_status = 1;
        $BNIDb->save();

        $BNIVirtualId = $BNIDb->id;
        Helper::LogPayment("Save To BNI Virtual Account. Id ".$BNIVirtualId,'bni');

        $paramHistory=[];
        $paramHistory['amount'] = $transactionAmount;
        $paramHistory['customerName'] = $customerName;
        $paramHistory['datetimeExpired'] = $datetimeExpired;
        $paramHistory['description'] = $description;

        // insert history
        Helper::LogPayment("Save History BNI VA. ".json_encode($paramHistory),'bni');
        BNIVirtualAccountHistory::createHistory($BNIVirtualId,$paramHistory);

        $response->isSuccess = true;
        $response->BNIVaId = $BNIDb->id;
        Helper::LogPayment("End Create VA",'bni');
        return $response;
    }

    public static function inquiryVA($paymentId){
        Helper::LogPayment("Begin Inquiry BNI",'bni');
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        // get Client Transaction DB
        $clientTransactionDb = ClientTransaction::where('payment_id',$paymentId)->first();
        if (!$clientTransactionDb){
            $response->errorMsg = 'Invalid Payment Id';
            return $response;
        }
        $clientTransactionId = $clientTransactionDb->id;
        // get BNI VA
        $BNIVirtualDb = $clientTransactionDb->BNIVirtualAccount;
        $BNIVirtualId = $BNIVirtualDb->id;

        // create param
        $param = [];
        $param['type'] = 'inquirybilling';
        $param['trx_id'] = $paymentId;

        Helper::LogPayment("Param ".json_encode($param),'bni');

        $apiBNI = new \App\Http\Libraries\BNIVirtualAccount();
        $apiResponse = $apiBNI->postVA($param);
        if (!$apiResponse->isSuccess){
            Helper::LogPayment("Failed ".$apiResponse->errorMsg,'bni');
            $response->errorMsg = $apiResponse->errorMsg;
            return $response;
        }
        $apiResponseData = $apiResponse->data;
        Helper::LogPayment("Response Parsed ".json_encode($apiResponseData),'bni');

        $virtualAccount = $apiResponseData['virtual_account'];
        $transactionAmount = $apiResponseData['trx_amount'];
        $paymentNtb = $apiResponseData['payment_ntb'];
        $paymentAmount = $apiResponseData['payment_amount'];
        $vaStatus = $apiResponseData['va_status'];
        $billingType = $apiResponseData['billing_type'];
        $datetimeExpired = date('Y-m-d H:i:s',strtotime($apiResponseData['datetime_expired_iso8601']));
        $datetimePayment = empty($apiResponseData['datetime_payment_iso8601']) ? null : date('Y-m-d H:i:s',strtotime($apiResponseData['datetime_payment_iso8601']));

        Helper::LogPayment("Update BNI Virtual Account DB",'bni');
        // update BNI Virtual Account DB
        $BNIVirtualDb = self::find($BNIVirtualId);
        $BNIVirtualDb->datetime_expired = $datetimeExpired;
        $BNIVirtualDb->datetime_payment = $datetimePayment;
        $BNIVirtualDb->transaction_amount = $transactionAmount;
        $BNIVirtualDb->payment_amount = $paymentAmount;
        $BNIVirtualDb->billing_type = $billingType;
        $BNIVirtualDb->virtual_account_number = $virtualAccount;
        $BNIVirtualDb->va_status = $vaStatus;
        $BNIVirtualDb->payment_ntb = $paymentNtb;
        $BNIVirtualDb->save();

        // if NTB present paid
        if (!empty($paymentNtb)){
            // get history with ntb
            $BNIVirtualHistoryDb = BNIVirtualAccountHistory::where('bni_virtual_accounts_id',$BNIVirtualDb->id)->where('payment_ntb',$paymentNtb)->first();
            if ($BNIVirtualHistoryDb){
                $currentAmount = $BNIVirtualHistoryDb->amount;
                $currentDateTimeExpired = $BNIVirtualHistoryDb->datetime_expired;
                $currentCumulative = $BNIVirtualHistoryDb->cumulative_payment_amount;

                $isChange = false;
                if ($currentAmount!= $transactionAmount) $isChange = true;
                if ($currentDateTimeExpired != $datetimeExpired) $isChange = true;
                if ($currentCumulative != $paymentAmount) $isChange = true;

                if ($isChange){
                    Helper::LogPayment("Update BNI Virtual Account History DB",'bni');
                    // there is change, insert new history
                    $paramHistory=[];
                    $paramHistory['amount'] = $transactionAmount;
                    $paramHistory['customerName'] = $clientTransactionDb->customer_name;
                    $paramHistory['datetimeExpired'] = $datetimeExpired;
                    $paramHistory['description'] = $clientTransactionDb->description;
                    $paramHistory['cumulativeAmount'] = $paymentAmount;

                    // insert history
                    Helper::LogPayment("Save History BNI VA. ".json_encode($paramHistory),'bni');
                    BNIVirtualAccountHistory::createHistory($BNIVirtualId,$paramHistory);
                }
            }
        }


        $response->isSuccess = true;
        $response->BNIVaId = $BNIVirtualDb->id;
        Helper::LogPayment("End Inquiry and Update VA",'bni');
        return $response;
    }

    /*Relationship*/
    public function clientTransaction(){
        return $this->belongsTo(ClientTransaction::class,'client_transactions_id','id');
    }

    public function BNIVirtualAccountHistory(){
        return $this->hasMany(BNIVirtualAccountHistory::class,'bni_virtual_accounts_id','id');
    }
}
