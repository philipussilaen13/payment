<?php

namespace App\Http\Models\apiV1;

use App\Http\Helper\Helper;
use App\Http\Libraries\BNIYapAPI;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use LaravelQRCode\Facades\QRCode;

class BNIYap extends Model
{
    protected $table = 'bni_yaps';

    /**
     * Create Yap Transaction
     * @param Request $request
     * @param $clientTransactionId
     * @return \stdClass
     */
    public static function createYapTransaction(Request $request, $clientTransactionId){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->bniYapId = null;
        $response->url = null;

        Helper::LogPayment("Begin Create Transaction",'yap');

        $clientTransactionDb = ClientTransaction::find($clientTransactionId);
        if (!$clientTransactionDb){
            $response->erroMsg = 'Invalid Transaction';
            return $response;
        }

        $paymentId = $clientTransactionDb->payment_id;
        $transactionAmount = $clientTransactionDb->total_amount;
        $locationId = $clientTransactionDb->location_id;

        $locationDb = Location::find($locationId);
        if ($locationDb){
            $locationId = $locationDb->location_id;
        }

        // create expired date time
        $expiredDateTime = date('Y-m-d H:i:s',strtotime("+24 hours"));

        // push to BNI API
        Helper::LogPayment("Push To API $paymentId,$transactionAmount,$locationId",'yap');

        $bniYapApi = new BNIYapAPI();
        $createQr = $bniYapApi->createQrCode($paymentId,$transactionAmount,$locationId);
        if (!$createQr->isSuccess){
            Helper::LogPayment("Failed Create Transaction. Failed: $createQr->errorMsg",'yap');
            $response->errorMsg = $createQr->errorMsg;
            return $response;
        }
        $qrData = $createQr->data;

        Helper::LogPayment("Success Create QR data $qrData",'yap');

        // save to DB
        $bniYapDb = new self();
        $bniYapDb->client_transactions_id = $clientTransactionId;
        $bniYapDb->transaction_amount = $transactionAmount;
        $bniYapDb->qr_data = $qrData;
        $bniYapDb->status = 'CREATED';
        $bniYapDb->expired_datetime = $expiredDateTime;
        $bniYapDb->save();
        Helper::LogPayment("Success Save DB",'yap');

        // save image QR
        $fileName = "$paymentId.png";
        $path = public_path()."/img/payment/yap/$fileName";
        QRCode::text($qrData)->setSize(7)->setOutfile($path)->png();

        // set url
        $url = url("img/payment/yap/$fileName");

        $response->isSuccess = true;
        $response->bniYapId = $bniYapDb->id;
        $response->url = $url;
        Helper::LogPayment("Finish Create Transaction",'yap');

        return $response;
    }

    public static function inquiryYap($paymentId){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->bniYapId = null;

        // get client Transaction DB
        $clientTransactionDb = ClientTransaction::with('BNIYap')
            ->where('payment_id',$paymentId)
            ->first();
        if (!$clientTransactionDb){
            $response->errorMsg = 'Client Transaction Not Found';
            return $response;
        }

        $bniYapDb = $clientTransactionDb->BNIYap;
        if (!$bniYapDb){
            $response->errorMsg = 'Invalid Payment ID and Method';
            return $response;
        }

        $paymentStatus = $clientTransactionDb->status;
        $clientTransactionId = $clientTransactionDb->id;
        Helper::LogPayment("Inquiry $paymentId $paymentStatus",'yap','inquiry');

        if ($paymentStatus == 'CREATED'){
            Helper::LogPayment('Begin Inquiry to YAP','yap','inquiry');
            // inquiry to server and Update if available
            $BNIYapAPI = new BNIYapAPI();
            // send inquiry
            $inquiry = $BNIYapAPI->inquiry($paymentId);
            if ($inquiry->isSuccess){
                $data = $inquiry->data;
                $appAccounPaymentId = isset($data->app_account_payment_id) ? $data->app_account_payment_id : null;
                $payType = isset($data->pay_type) ? $data->pay_type : null;
                $mVisaTransactionId = isset($data->mvisa_transaction_id) ? $data->mvisa_transaction_id : null;
                $merchantName = isset($data->merchant_name) ? $data->merchant_name : null;
                $merchantPan = isset($data->merchant_pan) ? $data->merchant_pan : null;
                $merchantPanRaw = isset($data->merchant_pan_raw) ? $data->merchant_pan_raw : null;
                $paymentAmount = isset($data->payment_amount) ? $data->payment_amount : null;
                $paymentAmountFee = isset($data->payment_amount_fee) ? $data->payment_amount_fee : null;
                $transactionDate = isset($data->transaction_date) ? date('Y-m-d H:i:s',strtotime($data->transaction_date)) : null;
                $consumerName = isset($data->consumer_name) ? $data->consumer_name : null;
                $consumerPan = isset($data->consumer_pan) ? $data->consumer_pan : null;
                $consumerPanRaw = isset($data->consumer_pan_raw) ? $data->consumer_pan_raw : null;
                $consumerPanMasking = isset($data->consumer_pan_masking) ? $data->consumer_pan_masking : null;
                $debitPan = isset($data->debit_pan) ? $data->debit_pan : null;

                $encodedData = json_encode($data);
                Helper::LogPayment("Update DB with result $encodedData",'yap','inquiry');
                // update BNI YAP DB
                $bniYapDb = BNIYap::find($bniYapDb->id);
                $bniYapDb->paid_amount = $paymentAmount;
                $bniYapDb->payment_amount_fee = $paymentAmountFee;
                $bniYapDb->pay_type = $payType;
                $bniYapDb->app_accounts_payment_id = $appAccounPaymentId;
                $bniYapDb->mvisa_transaction_id = $mVisaTransactionId;
                $bniYapDb->merchant_pan = $merchantPan;
                $bniYapDb->merchant_pan_raw = $merchantPanRaw;
                $bniYapDb->merchant_name = $merchantName;
                $bniYapDb->transaction_date = $transactionDate;
                $bniYapDb->consumer_name = $consumerName;
                $bniYapDb->consumer_pan = $consumerPan;
                $bniYapDb->consumer_pan_raw = $consumerPanRaw;
                $bniYapDb->consumer_pan_masking = $consumerPanMasking;
                $bniYapDb->debit_pan = $debitPan;
                $bniYapDb->status = 'PAID';
                $bniYapDb->inquiry_result = $encodedData;
                $bniYapDb->save();

                // update transactionDB
                $clientTransactionDb = ClientTransaction::find($clientTransactionDb->id);
                $clientTransactionDb->status = 'PAID';
                $clientTransactionDb->total_payment = $paymentAmount;
                $clientTransactionDb->last_payment = $paymentAmount;
                $clientTransactionDb->save();
                Helper::LogPayment("Update Transaction $clientTransactionId to 'PAID",'yap','inquiry');

                // insert history
                ClientTransactionHistory::insertHistory($clientTransactionId,'PAID','PAID',$paymentAmount);
                Helper::LogPayment("Insert History Transaction 'PAID",'yap','inquiry');

                // update client transaction detail to paid
                $clientTransactionDetailDb = ClientTransactionDetail::where('client_transactions_id',$clientTransactionId)->first();
                if ($clientTransactionDetailDb){
                    $clientTransactionDetailDb = ClientTransactionDetail::find($clientTransactionDetailDb->id);
                    $clientTransactionDetailDb->paid_amount = $paymentAmount;
                    $clientTransactionDetailDb->status = 'PAID';
                    $clientTransactionDetailDb->save();
                }
            } else {
                $errorMsg = $inquiry->errorMsg;
                Helper::LogPayment('Inquiry Failed. '.$errorMsg,'yap','inquiry');
            }
        }

        $response->isSuccess = true;
        $response->bniYapId = $bniYapDb->id;
        return $response;
    }

    /*Relationship*/
    public function clientTransaction(){
        return $this->belongsTo(ClientTransaction::class,'client_transactions_id','id');
    }
}
