<?php

namespace App\Http\Models\apiV1;

use App\Http\Helper\Helper;
use App\Http\Libraries\MolPay;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;

class MolPayTransaction extends Model
{
    protected $table = 'molpay_transactions';
    use SoftDeletes;

    /**
     * Create MolPay Transaction
     * @param Request $request
     * @param $clientTransactionId
     * @return \stdClass
     */
    public static function createMolPayTransaction(Request $request,$clientTransactionId,$molPayChannelCode='credit'){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->erroMsg = null;
        $response->molpayUrl = null;
        Helper::LogPayment("Begin Create Transaction",'molpay');

        $clientTransactionDb = ClientTransaction::find($clientTransactionId);
        if (!$clientTransactionDb){
            $response->erroMsg = 'Invalid Transaction';
            return $response;
        }

        $amount = $clientTransactionDb->total_amount;
        $currency = 'MYR';
        $channel = $molPayChannelCode;
        $orderId = $clientTransactionDb->payment_id;
        $billName = $request->input('customer_name');
        $billEmail = $request->input('customer_email');
        $billMobile = $request->input('customer_phone');
        $billDesc = $clientTransactionDb->description;
        $country = 'MY';

        // create parameter
        $param = [];
        $param['amount'] = $amount;
        $param['orderid'] = urlencode($orderId);
        $param['bill_name'] = urlencode($billName);
        $param['bill_email'] = urlencode($billEmail);
        $param['bill_mobile'] = urlencode($billMobile);
        $param['bill_desc'] = urlencode($billDesc);
        $param['country'] = $country;
        $param['currency'] = $currency;
        $param['channel'] = $channel;

        Helper::LogPayment("Create Param for URL ".json_encode($param),'molpay');

        $molPayLib = new MolPay();
        $result = $molPayLib->createURL($param);
        $url = $result->url;
        $vCode = $result->vCode;

        Helper::LogPayment("URL $url and vCode : $vCode",'molpay');

        // save to DB
        $db = new self();
        $db->client_transactions_id = $clientTransactionId;
        $db->amount = $amount;
        $db->currency = $currency;
        $db->channel = $channel;
        $db->v_code =$vCode;
        $db->url_request = $url;
        $db->save();

        Helper::LogPayment("Save To DB",'molpay');
        $response->isSuccess = true;
        $response->molpayUrl = $url;

        Helper::LogPayment("Response ".json_encode($response),'molpay');
        return $response;
    }
}
