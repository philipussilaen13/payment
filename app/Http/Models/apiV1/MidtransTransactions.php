<?php

namespace App\Http\Models\apiV1;

use Carbon\Carbon;
use App\Http\Helper\Helper;
use Illuminate\Http\Request;
use App\Http\Libraries\MidtransAPI;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class MidtransTransactions extends Model
{
    protected $table = 'midtrans_transactions';

    public static function createTransaction(Request $request, $clientTransactionId )
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->midtransId = null;
        $response->url = null;
        $methodCode = $request->input('method_code');

        // Logging to File
        Helper::LogPayment("Begin Create Transaction",'midtrans');
        $clientTransactionDb = ClientTransaction::find($clientTransactionId);
        
        // checking availability ID Transaction
        if (!$clientTransactionDb){
            $response->erroMsg = 'Invalid Transaction';
            return $response;
        }

        $paymentId = $clientTransactionDb->payment_id;
        $transactionAmount = $clientTransactionDb->total_amount;
        $locationId = $clientTransactionDb->location_id;

        $locationDb = Location::find($locationId);
        if ($locationDb){
            $locationId = $locationDb->location_id;
        }

        Helper::LogPayment("Push To API $paymentId,$transactionAmount,$locationId",'midtrans');
        $midtransApi = new MidtransAPI();

        if ($methodCode == 'MID-GOPAY') {
            $resultAPI   = $midtransApi->createGopayTrx($request, $paymentId);
            $midtransAPI = $resultAPI->data; 
            // set url
            $url_qr_gopay = $midtransAPI->actions[0]->url;
            $contents = file_get_contents($url_qr_gopay);
            Storage::put("public/gopay_qr/".$paymentId.".png", $contents);
            $url = url('/storage/gopay_qr/'.$paymentId.'.png');
            Helper::LogPayment("Success Create QR data ",'midtrans');

        } elseif ($methodCode == 'MID-SNAP') {
            $type = "mobile";
            $accessType = CompanyAccessToken::where('token', $request->input('token'))->first();
            if( strtolower($accessType->name) == 'locker' || strtolower($accessType->name) == 'global') {
                $type = "locker";
            }

            $resultAPI   = $midtransApi->createSnapTrx($request, $paymentId, $type);
            $midtransAPI = $resultAPI->data; 

            // set url
            $url = ($type == 'locker') ? $midtransAPI->qr_code_url : $midtransAPI->redirect_url;
        }

        if (!$resultAPI->isSuccess){
            Helper::LogPayment("Failed Create Transaction. Failed: $resultAPI->errorMsg",'midtrans');
            $response->errorMsg = $resultAPI->errorMsg;
            return $response;
        }
        

        // save to DB
        $midtransDb = new self();
        $midtransDb->client_transactions_id = $clientTransactionId;
        $midtransDb->gross_amount =  $request->input('amount');
        $midtransDb->method_code = $request->method_code;
        $midtransDb->order_id = $paymentId;        

        if ($methodCode == 'MID-SNAP') {
            $midtransDb->transaction_time = Carbon::now()->toDateString();
            $midtransDb->param_request = json_encode($midtransAPI);
            
        } else {
            $midtransDb->client_transactions_id = $clientTransactionId;
            $midtransDb->gross_amount = $midtransAPI->gross_amount;
            $midtransDb->status = $midtransAPI->transaction_status;
            $midtransDb->payment_type = $midtransAPI->payment_type;
            $midtransDb->transaction_id = $midtransAPI->transaction_id;
            $midtransDb->transaction_time = $midtransAPI->transaction_time;
            $midtransDb->status_message = $midtransAPI->status_message;
            $midtransDb->param_request = json_encode($midtransAPI);
        }

        $midtransDb->save();
        Helper::LogPayment("Success Save DB",'midtrans');

        $response->isSuccess = true;
        $response->midtransId = $midtransDb->id;
        $response->url = $url;
        Helper::LogPayment("Finish Create Transaction",'midtrans');
        return $response;        
    }

    public function notification(Request $request)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $method_code = $request->input('payment_type');
        $resAPI = json_decode($request->getContent());

        if (($resAPI->transaction_status == 'settlement') || ($resAPI->transaction_status == 'capture' && $resAPI->fraud_status == 'accept'))  {
            $midtransDB = MidtransTransactions::with('clientTransaction')
                        ->where('transaction_id', $resAPI->transaction_id)->first();
            if (empty($midtransDB)) {
                $midtransDB = MidtransTransactions::with('clientTransaction')
                        ->where('order_id', $resAPI->order_id)->first();
            }

            $clientTransactionId = $midtransDB->client_transactions_id;
            Helper::LogPayment('Begin Trx to midtrans','midtrans','trx');

            // CHECKING PAYMENT STATUS
            if ($midtransDB->status == 'pending') {
                $dataResApi = $resAPI;
                $encodedData = json_encode($dataResApi);
                Helper::LogPayment("Update DB with result $encodedData",'midtrans','trx');
            

                DB::beginTransaction();
                $midTransDb = MidtransTransactions::find($midtransDB->id);
                $midTransDb->status_message      = isset($dataResApi->status_message) ? $dataResApi->status_message : null;
                $midTransDb->payment_type        = isset($dataResApi->payment_type) ? $dataResApi->payment_type : null;
                $midTransDb->transaction_id      = isset($dataResApi->transaction_id) ? $dataResApi->transaction_id : null;
                $midTransDb->payment_amount      = isset($dataResApi->gross_amount) ? $dataResApi->gross_amount : null;
                $midTransDb->param_response      = $encodedData;
                $midTransDb->transaction_time    = isset($dataResApi->transaction_time) ? $dataResApi->transaction_time : null;
                $midTransDb->bank                = isset($dataResApi->bank) ? $dataResApi->bank : null;
                $midTransDb->approval_code       = isset($dataResApi->approval_code) ? $dataResApi->approval_code : null;
                $midTransDb->status              = isset($dataResApi->transaction_status) ? $dataResApi->transaction_status : null;
                $midTransDb->save();

                 // update transactionDB
                 $paymentAmount = isset($dataResApi->gross_amount) ? $dataResApi->gross_amount : null;
                 $clientTransactionDb = ClientTransaction::find($midtransDB->client_transactions_id);
                 $clientTransactionDb->status = 'PAID';
                 $clientTransactionDb->total_payment = $paymentAmount;
                 $clientTransactionDb->last_payment = $paymentAmount;
                 $clientTransactionDb->save();
                 Helper::LogPayment("Update Transaction $clientTransactionId to 'PAID",'midtrans','trx');
 
                 // insert history
                 ClientTransactionHistory::insertHistory($clientTransactionId,'PAID','PAID',$paymentAmount);
                 Helper::LogPayment("Insert History Transaction 'PAID",'midtrans','trx');
 
                 // update client transaction detail to paid
                 $clientTransactionDetailDb = ClientTransactionDetail::where('client_transactions_id',$clientTransactionId)->first();
                 if ($clientTransactionDetailDb){
                     $clientTransactionDetailDb = ClientTransactionDetail::find($clientTransactionDetailDb->id);
                     $clientTransactionDetailDb->paid_amount = $paymentAmount;
                     $clientTransactionDetailDb->status = 'PAID';
                     $clientTransactionDetailDb->save();
                 }

                 DB::commit();
                 if (!($clientTransactionDetailDb->save()) ||  !($clientTransactionDb->save()) || !($midTransDb->save())) {
                    $errorMsg = $inquiry->errorMsg;
                    Helper::LogPayment('Trx Failed. '.$errorMsg,'midtrans','trx');  
                 }
            }     
        }        
       
        Helper::LogPayment("Finish Create Transaction",'midtrans');
        $response->isSuccess = true;
        return $response;   
    }


    /* Relation to table ClientTransactions */
    public function clientTransaction(){
        return $this->belongsTo(\App\Http\Models\apiV1\ClientTransaction::class, 'client_transactions_id','id');
    }
}
