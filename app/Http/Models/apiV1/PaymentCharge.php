<?php

namespace App\Http\Models\apiV1;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PaymentCharge extends Model
{
    protected $table = 'payment_channel_charges';
    use SoftDeletes;
}
