<?php

namespace App\Http\Models\apiV1;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Company extends Model
{
    protected $table = 'companies';
    use SoftDeletes;

    /*Relationship*/
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function companyAccessToken(){
        return $this->hasMany(CompanyAccessToken::class,'companies_id','id');
    }
}
