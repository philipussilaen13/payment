<?php

namespace App\Http\Models\apiV1;

use App\Http\Helper\Helper;
use App\Http\Libraries\Doku\Doku_Api;
use App\Http\Libraries\Doku\Doku_Initiate;
use App\Http\Libraries\Doku\Doku_Library;
use Illuminate\Database\Eloquent\Model;

class DokuTransaction extends Model
{
    protected $table = 'doku_transactions';

    public static $availableVA = ['DOKU-BCA','DOKU-PERMATA','DOKU-MANDIRI','DOKU-ALFAMART','DOKU-INDOMART'];
    public static $availableDirect = ['DOKU-CREDIT'=>'15','DOKU-WALLET'=>'04'];

    /**
     * Create DOKU Virtual Account Transaction
     * @param $clientTransactionId
     * @param string $dokuChannelCode
     * @param int $expiredMinute
     * @param string $billingType
     * @param string $virtualAccount
     * @return \stdClass
     */
    public static function createDokuVATransaction($clientTransactionId,$dokuChannelCode='DOKU-PERMATA',$expiredMinute=360,$billingType='fixed',$virtualAccount=''){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->erroMsg = null;
        $response->dokuTransactionId = null;
        Helper::LogPayment("Begin Create Doku VA Transaction",'doku');

        $clientTransactionDb = ClientTransaction::find($clientTransactionId);
        if (!$clientTransactionDb){
            $response->erroMsg = 'Invalid Transaction';
            return $response;
        }

        $amount = $clientTransactionDb->total_amount;
        $paymentId = $clientTransactionDb->payment_id;
        $currency = '360';
        $customerName = $clientTransactionDb->customer_name;
        $customerEmail = $clientTransactionDb->customer_email;
        $customerPhone = $clientTransactionDb->customer_phone;
        $description = $clientTransactionDb->description;
        

        $dokuInitiate = new Doku_Initiate();
        $dokuAPI = new Doku_Api();

        $mallId = $dokuInitiate->mallId;
        $sharedKey = $dokuInitiate->sharedKey;

        if ($dokuChannelCode == 'DOKU-BCA'){
            if ($billingType == 'open'){
                $amount = 0;
                if (empty($virtualAccount)) {
                    $response->errorMsg = 'Virtual Number Required for Open Payment';
                    return $response;
                }
                $mallId = $dokuInitiate->bcaOpenMallId;
                $sharedKey = $dokuInitiate->bcaOpenSharedKey;
            } else {
                $mallId = $dokuInitiate->bcaCloseMallId;
                $sharedKey = $dokuInitiate->bcaCloseSharedKey;
            }
        }
        $amount = number_format($amount,2,'.','');

        $words = sha1($amount.$mallId.$sharedKey.$paymentId.$currency);

        Helper::LogPayment("Create Words param $amount.$mallId.$sharedKey.$paymentId.$currency Words : ".$words,'doku');

        $customerData = [];
        $customerData['name'] = $customerName;
        $customerData['data_phone'] = $customerPhone;
        $customerData['data_email'] = $customerEmail;
        $customerData['data_address'] = 'PopBox';

        if (empty($description)) $description = $paymentId;
        $tmpDescription = str_replace(',','',$description);
        $basket = "$tmpDescription,$amount,1,$amount";

        $requestDateTime = date('YmdHis');
        $sessionId = sha1($requestDateTime);
        $paymentData = [];
        $paymentData['req_mall_id'] = $mallId;
        $paymentData['req_chain_merchant'] = '1';
        $paymentData['req_amount'] = $amount;
        $paymentData['req_words'] = $words;
        $paymentData['req_trans_id_merchant'] = $paymentId;
        $paymentData['req_purchase_amount'] = $amount;
        $paymentData['req_request_date_time'] = date('YmdHis');
        $paymentData['req_session_id'] = $sessionId;
        $paymentData['req_email'] = $customerEmail;
        $paymentData['req_name'] = $customerName;
        $paymentData['req_basket'] = $basket;
        $paymentData['req_address'] = 'PopBox';
        $paymentData['req_mobile_phone'] = $customerPhone;
        $paymentData['req_expiry_time'] = $expiredMinute;
        if ($billingType == 'fixed'){
            if ($dokuChannelCode == 'DOKU-BCA') $paymentData['req_chain_merchant'] = 'NA';
            Helper::LogPayment("Generate PayCode Params ".json_encode($paymentData),'doku');
            if ($dokuChannelCode == 'DOKU-BCA') $apiResponse = $dokuAPI::doGeneratePaycode($paymentData,$dokuChannelCode);
            else $apiResponse = $dokuAPI::doGeneratePaycode($paymentData);
            Helper::LogPayment("Generate PayCode Response ".json_encode($apiResponse),'doku');

            if (empty($apiResponse)){
                $response->errorMsg = 'Empty api Response';
                Helper::LogPayment("Failed Generate PayCode. ".json_encode($response),'doku');
                return $response;
            }

            if($apiResponse->res_response_code != '0000'){
                $response->errorMsg = $apiResponse->res_response_code."-".$apiResponse->res_response_msg;
                Helper::LogPayment("Failed Generate PayCode. ".json_encode($response),'doku');
                return $response;
            }
            $paymentCode=$apiResponse->res_pay_code;
        } else {
            Helper::LogPayment("Open Amount use VA Number $virtualAccount",'doku');
            $paymentCode = $virtualAccount;
        }
        $prefixVA='';
        if ($dokuChannelCode == 'DOKU-PERMATA') $prefixVA = $dokuInitiate->permataCode;
        elseif ($dokuChannelCode == 'DOKU-MANDIRI') $prefixVA = $dokuInitiate->mandiriCode;
        elseif ($dokuChannelCode == 'DOKU-ALFAMART') $prefixVA =$dokuInitiate->alfamartCode;
        elseif ($dokuChannelCode == 'DOKU-INDOMART') $prefixVA = $dokuInitiate->indomartCode;
        elseif ($dokuChannelCode == 'DOKU-BCA') {
            if ($billingType == 'fixed') $prefixVA = $dokuInitiate->bcaCloseCode;
            if ($billingType == 'open') $prefixVA = $dokuInitiate->bcaOpenCode;
        }

        // save to DB
        $datetimeExpired = date('Y-m-d H:i:s',strtotime("+$expiredMinute minutes"));
        $dataDb = new self();
        $dataDb->client_transactions_id = $clientTransactionId;
        $dataDb->billing_type = $billingType;
        $dataDb->method_code = $dokuChannelCode;
        $dataDb->channel_prefix = $prefixVA;
        $dataDb->payment_code = $paymentCode;
        $dataDb->virtual_number = $prefixVA.$paymentCode;
        $dataDb->transaction_amount = $amount;
        $dataDb->customer_name = $customerName;
        $dataDb->session_id = $sessionId;
        $dataDb->words = $words;
        $dataDb->datetime_expired = $datetimeExpired;
        $dataDb->save();

        $response->isSuccess = true;
        $response->dokuTransactionId = $dataDb->id;

        Helper::LogPayment("Success Generate VA Doku ".json_encode($response),'doku');
        return $response;
    }

    /**
     * Create Doku Hosted Transaction
     * @param $clientTransactionId
     * @param string $dokuChannelCode
     * @param string $billingType
     * @return \stdClass
     */
    public static function createDokuHostedTransaction($clientTransactionId,$dokuChannelCode='DOKU-CREDIT',$expiredMinute=360,$billingType='fixed'){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->erroMsg = null;
        $response->dokuTransactionId = null;
        Helper::LogPayment("Begin Create Doku Hosted Transaction",'doku');

        $clientTransactionDb = ClientTransaction::find($clientTransactionId);
        if (!$clientTransactionDb){
            $response->erroMsg = 'Invalid Transaction';
            return $response;
        }

        $amount = $clientTransactionDb->total_amount;
        $paymentId = $clientTransactionDb->payment_id;
        $currency = '360';
        $customerName = $clientTransactionDb->customer_name;
        $customerEmail = $clientTransactionDb->customer_email;
        $customerPhone = $clientTransactionDb->customer_phone;
        $description = $clientTransactionDb->description;

        $dokuInitiate = new Doku_Initiate();
        $dokuAPI = new Doku_Api();
        $amount = number_format($amount,2,'.','');

        $params = [];
        $params['amount'] = $amount;
        $params['invoice'] = $paymentId;
        $words = Doku_Library::doCreateWords($params);
        Helper::LogPayment("Create Words param ".json_encode($params)." Words : ".$words,'doku');

        $availableHosted = self::$availableDirect;
        $paymentChannel = $availableHosted[$dokuChannelCode];

        $requestDateTime = date('YmdHis');
        $sessionId = sha1($requestDateTime);

        if (empty($description)) $description = $paymentId;
        $tmpDescription = str_replace(',','',$description);
        $basket = "$tmpDescription,$amount,1,$amount";

        $paymentData = [];
        $paymentData['MALLID'] = $dokuInitiate->mallId;
        $paymentData['BASKET'] = $basket;
        $paymentData['CHAINMERCHANT'] = 'NA';
        $paymentData['AMOUNT'] = $amount;
        $paymentData['PURCHASEAMOUNT'] = $amount;
        $paymentData['TRANSIDMERCHANT'] = $paymentId;
        $paymentData['WORDS'] = $words;
        $paymentData['CURRENCY'] = '360';
        $paymentData['PURCHASECURRENCY'] = '360';
        $paymentData['COUNTRY'] = 'ID';
        $paymentData['SESSIONID'] = $sessionId;
        $paymentData['REQUESTDATETIME'] = date('YmdHis');
        $paymentData['NAME'] = $customerName;
        $paymentData['EMAIL'] = empty($customerEmail) ? 'admin@popbox.asia' : $customerEmail;
        $paymentData['PAYMENTCHANNEL'] = $paymentChannel;

        Helper::LogPayment("Generate Form Params ".json_encode($paymentData),'doku');

        // save to DB
        $dataDb = new self();
        $dataDb->client_transactions_id = $clientTransactionId;
        $dataDb->billing_type = $billingType;
        $dataDb->method_code = $dokuChannelCode;
        $dataDb->transaction_amount = $amount;
        $dataDb->customer_name = $customerName;
        $dataDb->session_id = $sessionId;
        $dataDb->words = $words;
        $dataDb->form_params = json_encode($paymentData);
        $dataDb->save();

        $response->isSuccess = true;
        $response->dokuTransactionId = $dataDb->id;

        Helper::LogPayment("Success Generate VA Doku ".json_encode($response),'doku');
        return $response;
    }
}
