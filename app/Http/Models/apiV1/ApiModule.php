<?php

namespace App\Http\Models\apiV1;

use Illuminate\Database\Eloquent\Model;

class ApiModule extends Model
{
    protected $table = 'api_modules';
}
