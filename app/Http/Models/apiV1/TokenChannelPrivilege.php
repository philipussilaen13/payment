<?php

namespace App\Http\Models\apiV1;

use function foo\func;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TokenChannelPrivilege extends Model
{
    protected $table = 'token_channel_privileges';
    use SoftDeletes;

    /**
     * Get Available Payment Channel
     * @param $companyAccessToken
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public static function getAvailablePaymentChannel($companyAccessToken){
        $paymentChannelDb = CompanyAccessToken::with('paymentChannels')
            ->where('token',$companyAccessToken)
            ->get();

        return $paymentChannelDb;
    }
}
