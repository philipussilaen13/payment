<?php

namespace App\Http\Models\apiV1;

use Illuminate\Database\Eloquent\Model;

class CheckUpdateTemp extends Model
{
    protected $table = 'check_update_temp';
}
