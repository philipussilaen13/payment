<?php

namespace App\Http\Models\apiV1;

use Carbon\Carbon;
use App\Http\Helper\Helper;
use Illuminate\Http\Request;
use LaravelQRCode\Facades\QRCode;
use App\Http\Libraries\OttoPayAPI;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class OttopayTransactions extends Model
{
    protected $table = 'ottopay_transactions';

    public static function createTransaction(Request $request, $clientTransactionId, $client )
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->ottoPayId = null;
        $response->url = null;

        // Logging to File
        Helper::LogPayment("Begin Create Transaction",'ottopay');
        $clientTransactionDb = ClientTransaction::find($clientTransactionId);
        // checking availability ID Transaction
        if (!$clientTransactionDb){
            $response->erroMsg = 'Invalid Transaction';
            return $response;
        }

        $paymentId = $clientTransactionDb->payment_id;
        $transactionAmount = $clientTransactionDb->total_amount;
        $locationId = $clientTransactionDb->location_id;

        $locationDb = Location::find($locationId);
        if ($locationDb){
            $locationId = $locationDb->location_id;
        }

         // Start to Get Billing Id From OttoPay
         $postData = [
            "channelId" => "PEDE",
            "paymentType" => "qrCode",
            "orderId" => "".$paymentId."",
            "grossAmount" => $request->amount,
            "timestamp" => time(),
            "expiryDuration" => 1,
            "expiryUnit" => "DAY"
        ];

        Helper::LogPayment("Push To API $paymentId,$transactionAmount,$locationId",'ottopay');

        $ottoPayApi = new OttoPayAPI();
        $createBillingId = $ottoPayApi->postRequestApi($postData, $client);
        if (!$createBillingId->isSuccess){
            Helper::LogPayment("Failed Create Transaction. Failed: $createBillingId->errorMsg",'ottopay');
            $response->errorMsg = $createBillingId->errorMsg;
            return $response;
        }
        $QRcode = json_encode($createBillingId->data->string);
        Helper::LogPayment("Success Create QR data $QRcode",'ottopay');
        // End Process

        // save to DB
        $ottoPayDb = new self();
        $ottoPayDb->client_transactions_id = $clientTransactionId;
        $ottoPayDb->transaction_amount = $transactionAmount;
        $ottoPayDb->status_payment = 'Pending';
        $ottoPayDb->client = $client;
        $ottoPayDb->method_code = $request->method_code;
        $ottoPayDb->customer_name = $request->customer_name;
        $ottoPayDb->billing_type = $request->billing_type;        
        $ottoPayDb->billing_id = $createBillingId->data->billingId;
        $ottoPayDb->datetime_expired = Carbon::now()->addMinutes(3)->toDateTimeString();
        $ottoPayDb->param_request = json_encode($postData);
        $ottoPayDb->save();
        Helper::LogPayment("Success Save DB",'ottopay');

        // save image QR
        $fileName = "OTTO-".$createBillingId->data->billingId.".png";
        $path = public_path()."/img/payment/ottopay/$fileName";
        QRCode::text($createBillingId->data->string)->setSize(7)->setOutfile($path)->png();

        // set url
        $masterClient = MasterClient::where('id', $client)->first();
        $url = url("img/payment/ottopay/$fileName");
        if ($masterClient != null) {
            if ($masterClient->client_name == "KIMONU"){
                $url = $createBillingId->data->string;
            }
        }

         $response->isSuccess = true;
         $response->ottoPayId = $ottoPayDb->id;
         $response->url = $url;
         Helper::LogPayment("Finish Create Transaction",'ottopay');
         return $response;        
    }


    public static function inquiryOttoPayment(Request $request, $paymentId, $client)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->ottoPayId = null;

        $transactionStatus = $request->input('transactionStatus', null);

        // get client Transaction DB
        $clientTransactionDb = ClientTransaction::with('OttoPay')
            ->where('payment_id',$paymentId)
            ->first();

        if (!$clientTransactionDb){
            $response->errorMsg = 'Client Transaction Not Found';
            return $response;
        }

        $ottoPayDb = $clientTransactionDb->OttoPay;
        if (!$ottoPayDb){
            $response->errorMsg = 'Invalid Payment ID and Method';
            return $response;
        }

        $paymentStatus = $clientTransactionDb->status;
        $clientTransactionId = $clientTransactionDb->id;
        Helper::LogPayment("Inquiry $paymentId $paymentStatus",'ottopay','inquiry');
        
        if ($paymentStatus == 'CREATED') {
            Helper::LogPayment('Begin Inquiry to OttoPay','ottopay','inquiry');

            // CHECKING PAYMENT STATUS
            if ($transactionStatus == 'SUCCESS') {
                $inquiryOttoPay = new \stdClass();
                $inquiryOttoPay->isSuccess = true;
                $inquiryOttoPay->data = json_decode($request->getContent());
            } else {
                // GET Status payement with OttoPay using API
                $ottoPayApi     = new OttoPayAPI();
                $inquiryOttoPay = $ottoPayApi->inquiryOttoPay($paymentId, $client);
            }


            if ($inquiryOttoPay->isSuccess) {
                $dataResApi = $inquiryOttoPay->data;
                $encodedData = json_encode($dataResApi);
                Helper::LogPayment("Update DB with result $encodedData",'ottopay','inquiry');
                
                // Time Convert Manual
                if (isset($dataResApi->transactionTime)) {
                    $datePay = str_replace(" ", "-", substr($dataResApi->transactionTime, 0, 10));
                    $timePay = substr($dataResApi->transactionTime, -8);
                    $dateTimePay = date('Y-m-d H:i:s',strtotime( $datePay." ".$timePay ));
                }

                DB::beginTransaction();
                $ottoPayDB = OttopayTransactions::find($ottoPayDb->id);
                $ottoPayDB->reference_number    = isset($dataResApi->referenceNumber) ? $dataResApi->referenceNumber : null;
                $ottoPayDB->last_payment_amount = isset($dataResApi->grossAmount) ? $dataResApi->grossAmount : null;
                $ottoPayDB->param_response      = $encodedData;
                $ottoPayDB->payment_date        = isset($dataResApi->transactionTime) ? $dateTimePay : null;
                $ottoPayDB->status_payment      = (isset($dataResApi->transactionStatus) && ($dataResApi->transactionStatus == 'SUCCESS')) ? 'Success' : null;
                $ottoPayDB->save();

                 // update transactionDB
                 $paymentAmount = isset($dataResApi->grossAmount) ? $dataResApi->grossAmount : null;
                 $clientTransactionDb = ClientTransaction::find($clientTransactionDb->id);
                 $clientTransactionDb->status = 'PAID';
                 $clientTransactionDb->total_payment = $paymentAmount;
                 $clientTransactionDb->last_payment = $paymentAmount;
                 $clientTransactionDb->save();
                 Helper::LogPayment("Update Transaction $clientTransactionId to 'PAID",'ottopay','inquiry');
 
                 // insert history
                 ClientTransactionHistory::insertHistory($clientTransactionId,'PAID','PAID',$paymentAmount);
                 Helper::LogPayment("Insert History Transaction 'PAID",'ottopay','inquiry');
 
                 // update client transaction detail to paid
                 $clientTransactionDetailDb = ClientTransactionDetail::where('client_transactions_id',$clientTransactionId)->first();
                 if ($clientTransactionDetailDb){
                     $clientTransactionDetailDb = ClientTransactionDetail::find($clientTransactionDetailDb->id);
                     $clientTransactionDetailDb->paid_amount = $paymentAmount;
                     $clientTransactionDetailDb->status = 'PAID';
                     $clientTransactionDetailDb->save();
                 }

                DB::commit();

            } else {
                $errorMsg = $inquiry->errorMsg;
                Helper::LogPayment('Inquiry Failed. '.$errorMsg,'ottopay','inquiry');
            }       
        }

        $response->isSuccess = true;
        $response->ottoPayId = $ottoPayDb->id;
        return $response;
    }

     /* Relation to table ClientTransactions */
    public function clientTransaction(){
        return $this->belongsTo(\App\Http\Models\apiV1\ClientTransaction::class, 'client_transactions_id','id');
    }

}
