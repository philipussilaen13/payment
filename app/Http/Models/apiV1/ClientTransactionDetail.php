<?php

namespace App\Http\Models\apiV1;

use Illuminate\Database\Eloquent\Model;

class ClientTransactionDetail extends Model
{
    protected $table = 'client_transaction_details';

    /**
     * Insert Transaction Detail , description, amount, status
     * @param $clientTransactionId
     * @param array $items
     */
    public static function insertTransactionDetail($clientTransactionId,$items=[]){
        foreach ($items as $item){
            $detailDb = new self();
            $detailDb->client_transactions_id = $clientTransactionId;
            $detailDb->sub_payment_id = empty($item->sub_payment_id) ? null : $item->sub_payment_id;
            $detailDb->description = substr($item->description,0,150);
            $detailDb->amount = $item->amount;
            $detailDb->status = $item->status;
            if (isset($item->paid_amount)){
                $detailDb->paid_amount = $item->paid_amount;
            }
            if (isset($item->channel_admin_fee)){
                $detailDb->channel_admin_fee = $item->channel_admin_fee;
            }
            if (isset($item->customer_admin_fee)){
                $detailDb->customer_admin_fee = $item->customer_admin_fee;
            }
            $detailDb->save();
        }
        return;
    }

    /*Relationship*/
    public function clientTransaction(){
        return $this->belongsTo(ClientTransaction::class,'client_transactions_id','id');
    }

    public function clientRepushCallback(){
        return $this->belongsTo(ClientPushCallback::class,'id','client_transaction_details_id');
    }
}
