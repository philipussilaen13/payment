<?php 

namespace App\Http\Libraries;

use Illuminate\Support\Facades\DB;
use App\Http\Models\apiV1\CurlResponse;
use App\Http\Helper\apiV1\ApiHelper;
use App\Http\Models\apiV1\MasterClient;

class OttoPayApi 
{

    private $ottoUrl    = null;
    private $serverKey  = null;
    private $merchantId = null;
    private $stringUrl  = null;

    public function __construct()
    {
        $this->ottoUrl    = env('OTTOPAY_API_URL');
        $this->serverKey  = env('OTTOPAY_SERVER_KEY'); 
        $this->serverKeyQR  = env('OTTOPAY_SERVER_KEY_STRINGBUILDER'); 
        $this->merchantId = env('OTTOPAY_MERCHANT_ID', 'POPBOX'); 
        $this->terminalId = env('OTTOPAY_TERMINAL_ID', 'POPBOX'); 
        $this->stringUrl  = env('OTTOPAY_STRING_BUILDER', "http://fingate.ottopay.id:6975/api/v1/issuer/emvcojson"); 

    }

    public function postRequestApi($params = [], $client)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->responseCode = "200";
        $response->errorMsg = null;
        $ch = curl_init();

        $credential = $this->credential($client, $params["timestamp"]);

         $header[] = "Content-Type: application/json";
         $header[] = "Timestamp: ". $params["timestamp"];
         $header[] = "Authorization: Basic ". base64_encode($credential->merchant); // waiting from ottopay
         $header[] = "Signature: ". $credential->signature; // using hashing HMAC256 
         
        // Institution ID: M-0000P0PB
        // $header[] = "Content-Type: application/json";
        // $header[] = "Timestamp: 1531731860";
        // $header[] = "Authorization: Basic TS0wMDAwUDBQQg==";
        // $header[] = "Signature: e1R7j62b+m4fut8MVijYeeEHi8Pe2hVra/uZMI0ahBU=";
        
        curl_setopt($ch, CURLOPT_URL, $this->ottoUrl."/charge"); // Create Payments
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        
        $resApi = curl_exec($ch);
        curl_close($ch);

        if ((json_decode($resApi)->responseCode) == "401" || $resApi != true) {
            $response->responseCode = "401";
            $response->errorMsg = (json_decode($resApi)->responseDescription);
            $this->logApiFile("Error Message when Create Billing ".$resApi);
            return $response;
        }

        $stringBuilder = $this->stringBuilder($resApi, $params['timestamp']);

        $this->saveResponse($this->ottoUrl,$params,$resApi);
        $message = $resApi;
        $this->logApiFile("Create Code Billing Success ".$message);
        $data = json_encode([
            'billingId' => json_decode($resApi)->billingId,
            'string' => $stringBuilder->data->data
        ]);
        
        $response->isSuccess = true;
        $response->data = json_decode($data);
        return $response;
    }

    public function inquiryOttoPay($orderId, $client)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->responseCode = "200";
        $response->errorMsg = null;
        $url = $this->ottoUrl."/status/".$orderId;
        $ch = curl_init();

        $timestamps = time();
        $credential = $this->credential($client, $timestamps);

        $header[] = "Content-Type: application/json";
        $header[] = "Timestamp: ". $timestamps;
        $header[] = "Authorization: Basic ". base64_encode($credential->merchant); // waiting from ottopay
        $header[] = "Signature: ". $credential->signature; // using hashing HMAC256 

        // Institution ID: M-0000P0PB
        // $header[] = "Content-Type: application/json";
        // $header[] = "Timestamp: 1531731860";
        // $header[] = "Authorization: Basic TS0wMDAwUDBQQg==";
        // $header[] = "Signature: e1R7j62b+m4fut8MVijYeeEHi8Pe2hVra/uZMI0ahBU=";

        curl_setopt($ch, CURLOPT_URL, $url); // Inquiry Payment (Check Status Payments)
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);

        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

        $resApi = curl_exec($ch);
        curl_close($ch);

        if ((json_decode($resApi)->responseCode) == "401") {
            $response->responseCode = "401";
            $response->errorMsg = (json_decode($resApi)->responseDescription);
            $this->logApiFile("Error Message when Inquiry Payments ".$resApi);
            return $response;
        }

        $message = $resApi;
        $this->logApiFile("Inquiry Payments is Success ".$message);

        $response->isSuccess = true;
        $response->data = json_decode($resApi);
        return $response;
    }

    public function credential($client, $timestamps)
    {
        $credential = new \stdClass();
        $credential->signature = null;
        $credential->merchant = null;
        $credential->serverkey = null;

        $clientDb = MasterClient::where('id', $client)->first();
        $clientName = $clientDb->client_name;
        if ($clientName == 'WARUNG') {
            $serverKey = \Config::get('ottopay.warung.server_key');
            $merchant = \Config::get('ottopay.warung.merchant');
        } elseif ($clientName == 'KIMONU') {
            $serverKey = $this->serverKey;
            $merchant = \Config::get('ottopay.kimonu.merchant');
        } else {
            $serverKey = $this->serverKey;
            $merchant  = $this->merchantId;
        }

        $signature = ApiHelper::createSignature($merchant, $timestamps, $serverKey);
       
        $credential->signature = $signature;
        $credential->merchant = $merchant;
        $credential->serverkey = $serverKey;

        return $credential;
    }

    public function stringBuilder($resCreatePayment, $timestamps)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->responseCode = "200";
        $response->errorMsg = null;
        $url  = $this->stringUrl;
        $data = json_decode($resCreatePayment);
        $serverKey = $this->serverKeyQR;

        $createString = [
            "pan" => "310509039",
            "trx_amount" => $data->grossAmount,
            "terminal_id" => $this->terminalId,
            "billing_id_partner" => $data->billingId
        ];

        $signature = ApiHelper::signatureStringBuilder(json_encode($createString), $timestamps, $serverKey);
        $bodyParam = json_encode($createString);

        $header[] = "Content-Type: application/json";
        $header[] = "Timestamp: ". $timestamps;
        $header[] = "Authorization: Basic T1RUT1BBWQ==";
        $header[] = "Signature: ". $signature;
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url); // Inquiry Payment (Check Status Payments)
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $bodyParam);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

        $resApi = curl_exec($ch);
        curl_close($ch);
        
        if ((json_decode($resApi)->status) != "200") {
            $response->responseCode = "401";
            $response->errorMsg = (json_decode($resApi)->message);
            $this->logApiFile("Error Message when Create Billing ".$resApi);
            return $response;
        }

        $this->saveResponse($this->ottoUrl,$bodyParam,$resApi);
        $message = $resApi;
        $this->logApiFile("Create String QR Ottopay Success ".$message);

        $response->isSuccess = true;
        $response->data = json_decode($resApi);
        return $response;
    }

    /**
     * Save Response from APIs
     * @param $url
     * @param $param
     * @param $response
     */
    private function saveResponse($url,$param,$response){
        $data = new CurlResponse();
        $data->api_url = $url;
        $data->api_send_data = json_encode($param);
        $data->api_response = $response;
        $data->save();
        return;
    }

    /**
     * Log
     * @param $message
     * @param string $type
     */
    private function logApiFile($message,$type='api'){
        $message = " $message\n";
        $f = fopen(storage_path().'/payment/ottopay/'.$type.date('Y.m.d.').'log','a');
        fwrite($f,date('H:i:s')." $message");
        fclose($f);
    }
}




?>