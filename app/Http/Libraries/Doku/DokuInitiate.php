<?php
/**
 * Initiate Doku data's
 */

namespace App\Http\Libraries\Doku;
class Doku_Initiate {
	 
	//local
	const prePaymentUrl = 'http://pay.doku.com/api/payment/PrePayment';
	const paymentUrl = 'http://pay.doku.com/api/payment/paymentMip';
	const directPaymentUrl = 'http://pay.doku.com/api/payment/PaymentMIPDirect';
	const generateCodeUrl = 'https://pay.doku.com/api/payment/DoGeneratePaycodeVA';
	const generateCodeBCA = 'https://pay.doku.com/api/payment/doGeneratePaymentCode';
	const redirectPaymentUrl = 'http://pay.doku.com/api/payment/doInitiatePayment';
	const captureUrl = 'http://pay.doku.com/api/payment/DoCapture';

	public $sharedKey; //doku's merchant unique key
	public $mallId; //doku's merchant id
    public $permataCode;
    public $alfamartCode;
    public $mandiriCode;
    public $indomartCode;
    public $bcaOpenMallId;
    public $bcaOpenSharedKey;
    public $bcaOpenCode;
    public $bcaCloseMallId;
    public $bcaCloseSharedKey;
    public $bcaCloseCode;

    function __construct()
    {
        $this->mallId = env('DOKU_MALLID');
        $this->sharedKey = env('DOKU_SHARED_KEY');
        $this->permataCode = env('DOKU_PERMATA');
        $this->alfamartCode = env('DOKU_ALFAMART');
        $this->mandiriCode = env('DOKU_MANDIRI');
        $this->indomartCode = env('DOKU_INDOMART');

        $this->bcaOpenMallId = env('DOKU_BCA_OPEN_MALLID');
        $this->bcaOpenSharedKey = env('DOKU_BCA_OPEN_SHARED_KEY');
        $this->bcaOpenCode = env('DOKU_BCA_OPEN');

        $this->bcaCloseMallId = env('DOKU_BCA_CLOSE_MALLID');
        $this->bcaCloseSharedKey = env('DOKU_BCA_CLOSE_SHARED_KEY');
        $this->bcaCloseCode = env('DOKU_BCA_CLOSED');
    }
}
