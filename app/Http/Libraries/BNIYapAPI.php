<?php
/**
 * Created by PhpStorm.
 * User: arief
 * Date: 12/02/2018
 * Time: 11.01
 */

namespace App\Http\Libraries;


use App\Http\Models\apiV1\CurlResponse;

class BNIYapAPI
{
    private $pan = null;
    private $bniUrl = null;

    public function __construct()
    {
        $this->pan = env('BNI_YAP_PAN_ID',null);
        $this->bniUrl = env('BNI_YAP_PAN_URL',null);
    }

    /**
     * Create QR Code
     * @param $paymentId
     * @param $transactionAmount
     * @param $locationId
     * @return \stdClass
     */
    public function createQrCode($paymentId,$transactionAmount,$locationId){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = null;

        $this->logApiFile("Begin String Builder");

        // create parameter
        $parameter = [];
        $parameter['pan'] = $this->pan;
        $parameter['bill_id'] = $paymentId;
        $parameter['trx_amount'] = $transactionAmount;
        $parameter['terminal_id'] = $locationId;

        $this->logApiFile("Create QR with param ".json_encode($parameter));

        // send to BNI API
        $service = 'string-builder/request';
        $url = $this->bniUrl."/".$service;
        $apiResponse = $this->postAPI($url,$parameter);
        if (empty($apiResponse)){
            $this->logApiFile("Empty Response");
            $response->errorMsg = 'Failed to Push to BNI';
            return $response;
        }
        $apiResponseJson = json_decode($apiResponse);
        if ($apiResponseJson->status!=200){
            $this->logApiFile("Error Status ".$apiResponseJson->message);
            $response->errorMsg = 'Error Status '.$apiResponseJson->message;
            return $response;
        }
        $qrValue = $apiResponseJson->data;
        $this->logApiFile('Success Create with value '.$qrValue);

        $response->isSuccess = true;
        $response->data = $qrValue;
        return $response;
    }

    /**
     * Inquiry
     * @param $paymentId
     * @return \stdClass
     */
    public function inquiry($paymentId){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = null;

        $this->logApiFile("Begin String Builder");

        // create parameter
        $parameter = [];
        $parameter['merchant_pan'] = $this->pan;
        $parameter['bill_id'] = $paymentId;

        $this->logApiFile("Inquiry with param ".json_encode($parameter));

        // send to BNI API
        $service = 'inquiry/data';
        $url = $this->bniUrl."/".$service;
        $apiResponse = $this->postAPI($url,$parameter);
        if (empty($apiResponse)){
            $this->logApiFile("Empty Response");
            $response->errorMsg = 'Failed to Push to BNI';
            return $response;
        }
        $apiResponseJson = json_decode($apiResponse);
        if ($apiResponseJson->status!=200){
            $this->logApiFile("Error Status ".$apiResponseJson->message);
            $response->errorMsg = 'Error Status '.$apiResponseJson->message;
            return $response;
        }
        $data = $apiResponseJson->data;
        $this->logApiFile('Success Inquiry with value '.json_encode($data));

        $response->isSuccess = true;
        $response->data = $data;
        return $response;
    }

    /**
     * Post API with cUrl
     * @param $url
     * @param array $param
     * @return bool|mixed
     */
    private function postAPI($url, $param = []) {
        // $header[] = 'Content-Type: application/json';
        $header[] = "Accept-Encoding: gzip, deflate";
        $header[] = "Cache-Control: max-age=0";
        $header[] = "Connection: keep-alive";
        $header[] = "Accept-Language: en-US,en;q=0.8,id;q=0.6";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_VERBOSE, false);
        // curl_setopt($ch, CURLOPT_NOBODY, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_ENCODING, true);
        curl_setopt($ch, CURLOPT_AUTOREFERER, true);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 5);

        if (!empty($param))
        {
            // $post = json_encode($param);
            $post = http_build_query($param);
            $message = "$url $post";
            $this->logApiFile($message);

            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        }

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $rs = curl_exec($ch);

        if(empty($rs)){
            var_dump($rs, curl_error($ch));
            curl_close($ch);
            return false;
        }
        curl_close($ch);

        $this->saveResponse($url,$param,$rs);
        $message = $rs;
        $this->logApiFile($message);

        return $rs;
    }

    /**
     * Save Response from APIs
     * @param $url
     * @param $param
     * @param $response
     */
    private function saveResponse($url,$param,$response){
        $data = new CurlResponse();
        $data->api_url = $url;
        $data->api_send_data = json_encode($param);
        $data->api_response = $response;
        $data->save();
        return;
    }

    /**
     * Log
     * @param $message
     * @param string $type
     */
    private function logApiFile($message,$type='api'){
        $message = " $message\n";
        $f = fopen(storage_path().'/payment/yap/'.$type.date('Y.m.d.').'log','a');
        fwrite($f,date('H:i:s')." $message");
        fclose($f);
    }
}