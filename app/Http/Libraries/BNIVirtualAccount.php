<?php
/**
 * Created by PhpStorm.
 * User: arief
 * Date: 03/08/2017
 * Time: 10.03
 */

namespace App\Http\Libraries;


use App\Http\Models\apiV1\CurlResponse;

class BNIVirtualAccount
{
    public $clientId;
    public $secretKey;
    public $bniUrl;

    public function __construct()
    {
        $this->clientId = env('BNI_CLIENT_ID','261');
        $this->secretKey = env('BNI_SECRET_KEY','2bb1f6f01b7c374a15b24ddcd0fbd136');
        $this->bniUrl = env('BNI_URL','https://apibeta.bni-ecollection.com/');
    }

    /**
     * Create Virtual Account
     * @param array $parameter
     * @return \stdClass
     */
    public function postVA($parameter=[]){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = null;

        $parameter['client_id'] = $this->clientId;

        $this->logApiFile("Request Parameter ".json_encode($parameter));
        // hashing data
        $hashedData = BNIHashing::hashData($parameter,$this->clientId,$this->secretKey);
        $this->logApiFile("Request Hashed ".json_encode($hashedData));

        $sendData = [
            'client_id' => $this->clientId,
            'data' => $hashedData
        ];
        // push to BNI
        $apiResponse = $this->postAPI($this->bniUrl,$sendData);
        if (empty($apiResponse)){
            $response->errorMsg = 'Failed to Push to BNI';
            return $response;
        }
        $this->logApiFile("Response Hashed $apiResponse");
        $apiResponseJson = json_decode($apiResponse,true);
        if ($apiResponseJson['status'] !== '000') {
            //$response->errorMsg = 'Error Status '.$apiResponseJson['status'].' '.$apiResponseJson['message'];
            $response->errorMsg = 'Error Status '.$apiResponseJson['message'];
            return $response;
        }
        $apiResponseData = BniHashing::parseData($apiResponseJson['data'], $this->clientId, $this->secretKey);
        $this->logApiFile("Response Parsed ".json_encode($apiResponseData));

        $response->isSuccess = true;
        $response->data = $apiResponseData;
        return $response;
    }

    /**
     * Post API with cUrl
     * @param $url
     * @param array $param
     * @return bool|mixed
     */
    private function postAPI($url, $param = []) {
        $nowDate = date('Y.m.d');
        $usecookie = storage_path()."payment/bni/cookie-$nowDate.txt";
        $header[] = 'Content-Type: application/json';
        $header[] = "Accept-Encoding: gzip, deflate";
        $header[] = "Cache-Control: max-age=0";
        $header[] = "Connection: keep-alive";
        $header[] = "Accept-Language: en-US,en;q=0.8,id;q=0.6";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_VERBOSE, false);
        // curl_setopt($ch, CURLOPT_NOBODY, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_ENCODING, true);
        curl_setopt($ch, CURLOPT_AUTOREFERER, true);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 5);

        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.120 Safari/537.36");

        if (!empty($param))
        {
            $post = json_encode($param);
            $message = "$url $post";
            $this->logApiFile($message);

            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        }

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $rs = curl_exec($ch);

        if(empty($rs)){
            var_dump($rs, curl_error($ch));
            curl_close($ch);
            return false;
        }
        curl_close($ch);

        $this->saveResponse($url,$param,$rs);
        $message = $rs;
        $this->logApiFile($message);

        return $rs;
    }

    /**
     * Save Response from APIs
     * @param $url
     * @param $param
     * @param $response
     */
    private function saveResponse($url,$param,$response){
        $data = new CurlResponse();
        $data->api_url = $url;
        $data->api_send_data = json_encode($param);
        $data->api_response = $response;
        $data->save();
        return;
    }

    /**
     * Log
     * @param $message
     * @param string $type
     */
    private function logApiFile($message,$type='api'){
        $message = " $message\n";
        $f = fopen(storage_path().'/payment/bni/'.$type.date('Y.m.d.').'log','a');
        fwrite($f,date('H:i:s')." $message");
        fclose($f);
    }
}