<?php 

namespace App\Http\Libraries;

use App\Http\Models\apiV1\CurlResponse;
use App\Http\Helper\apiV1\ApiHelper;
use Illuminate\Http\Request;
use App\Http\Models\apiV1\TcashTransactions;
use Illuminate\Http\Response;
use View;

class TcashAPI 
{
    private $pgpTokenUrl    = null;
    private $webDirectUrl   = null;
    private $terminalId     = null;
    private $sigKey         = null;
    private $pwd            = null;
    private $userKey        = null;
    private $successUrl     = null;
    private $failedUrl      = null;

    public function __construct()
    {
        $this->pgpTokenUrl     = env('TCASH_PGPTOKEN_URL', null);
        $this->webDirectUrl    = env('TCASH_WEBDIRECTION_URL', null);
        $this->terminalId      = env('TCASH_TERMINAL', "testing_tcash_wco");
        $this->sigKey          = env('TCASH_SIGKEY', "wcotestsign");
        $this->pwd             = env('TCASH_PWD', "@wcotest12");
        $this->userKey         = env('TCASH_USERKEY', "wcotest1090");
        $this->successUrl      = env('TCASH_SUCCESS', 'http://paymentdev.popbox.asia/api/callback/tcash/webcheckout/success');
        $this->failedUrl       = env('TCASH_FAILED', 'http://paymentdev.popbox.asia/api/callback/tcash/webcheckout/failed');

    }

    public function getUrlCheckout(Request $request, $paymentId, $tcashId)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->responseCode = "200";
        $response->errorMsg = null;
        $url = "https://tcash.telkomsel.com/tcashpay/payment";

        $postData = [
            'trxId' => $paymentId,
            'terminalId' => $this->terminalId,
            'userKey' => $this->userKey,
            'password' => $this->pwd,
            'signature' => $this->sigKey,
            'total' => $request->amount,
            'successUrl' => $this->successUrl,
            'failedUrl' => $this->failedUrl,
            'items' => []
        ];

        $reqToken       = $this->pgpToken($postData, $tcashId);
        $decodeRefToken = json_decode($reqToken);
    
        $postData = [
            'trxId' => $paymentId,
            'total' => $request->amount,
            'terminalId' => $this->terminalId,
            'successUrl' => $this->successUrl,
            'failedUrl' => $this->failedUrl,
        ];

        $message = json_encode($reqToken);
        $this->saveResponse($this->webDirectUrl,$postData,$message);
        $this->logApiFile("Create Code Billing Success ".$message);
        $response->isSuccess = true;
        $response->data = [
            'token' => $decodeRefToken->pgpToken,
            'post' => $postData,
        ];
        return $response;        
    }

    public function pgpToken($postData, $tcashId)
    {
        $ch = curl_init();
        
        $post = http_build_query($postData, '', '&');

        $url  = "https://tcash.telkomsel.com:11080/tcash-api/api/payment?"; 
        curl_setopt($ch, CURLOPT_URL, $url); // Create Payments
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        
        $resApi = curl_exec($ch);
        curl_close($ch);

        $resToken = json_decode($resApi);

        if (isset($resToken->refNum)) {
           $tcashDb = new TcashTransactions();
           $updateRef = $tcashDb->updateRefNumber($resToken->refNum, $tcashId);
        }

        return $resApi;
    }

     /**
     * Save Response from APIs
     * @param $url
     * @param $param
     * @param $response
     */
    private function saveResponse($url,$param,$response){
        $data = new CurlResponse();
        $data->api_url = $url;
        $data->api_send_data = json_encode($param);
        $data->api_response = $response;
        $data->save();
        return;
    }

    /**
     * Log
     * @param $message
     * @param string $type
     */
    private function logApiFile($message,$type='api'){
        $message = " $message\n";
        $f = fopen(storage_path().'/payment/tcash/'.$type.date('Y.m.d.').'log','a');
        fwrite($f,date('H:i:s')." $message");
        fclose($f);
    }
}

?>