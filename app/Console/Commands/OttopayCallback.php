<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use GuzzleHttp\Client;
use App\Http\Helper\Helper;
use App\Console\Commands\DB;
use Illuminate\Console\Command;
use App\Jobs\CheckUpdatePayment;
use App\Http\Helper\apiV1\ApiHelper;
use App\Http\Models\apiV1\OttopayTransactions;
use App\Http\Models\apiV1\ClientTransaction;
use App\Http\Models\apiV1\CheckUpdateTemp;
use App\Http\Models\apiV1\MasterClient;
use GuzzleHttp\Exception\GuzzleException;


class OttopayCallback extends Command
{

    private $ottoUrl    = null;
    private $serverKey  = null;
    private $merchantId = null;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ottopay:paymentstatus';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get Payment Status';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {  
        $this->ottoUrl    = env('OTTOPAY_API_URL', "https://fintech-dev.pactindo.com:8443/api/v1/merchant");
        $this->serverKey  = env('OTTOPAY_SERVER_KEY', null); 
        $this->merchantId = env('OTTOPAY_MERCHANT_ID', null); 
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $url = $this->ottoUrl."/status/";
        $data = \DB::table('client_transactions')
                // ->select('payment_id')
                ->join('ottopay_transactions', 'client_transactions.id', '=', 'ottopay_transactions.client_transactions_id')
                ->where('ottopay_transactions.status_payment', 'Pending')
                ->whereNotNull('ottopay_transactions.billing_id')
                ->where('ottopay_transactions.datetime_expired','<', Carbon::now()->toDateTimeString())
                ->orderBy('ottopay_transactions.id', 'desc')
                ->get();     

        foreach ($data as $key => $dataPayment) {         
            $clientDb = MasterClient::where('id', $dataPayment->client)->first();
            $clientName = $clientDb->client_name;
            
            if ($clientName == 'WARUNG') {
                $serverKey = \Config::get('ottopay.warung.server_key');
                $merchant = \Config::get('ottopay.warung.merchant');
            } elseif ($clientName == 'KIMONU') {
                $serverKey = $this->serverKey;
                $merchant = \Config::get('ottopay.kimonu.merchant');
            } else {
                $serverKey = $this->serverKey;
                $merchant  = $this->merchantId;
            }

            $timestamp = time();
            $signature = ApiHelper::createSignature($merchant, $timestamp, $serverKey);
            $client = new Client([
                'base_uri' => $url, // Base URI is used with relative requests
                'headers' => array(
                        'Content-Type'       => 'application/json',
                        'Timestamp'          => $timestamp,
                        'Authorization'      => 'Basic '. base64_encode($merchant),
                        'Signature'          => $signature
                )
            ]);
            
            $orderId = $dataPayment->payment_id;
            // Send an asynchronous request.
            $request = new \GuzzleHttp\Psr7\Request('GET', $url.$orderId);
            $promise = $client->sendAsync($request)->then(function ($response) use ($dataPayment) {
                $res = json_decode($response->getBody());
                // Helper::LogPayment("Check Ottopay payment number ". $dataPayment->payment_id ." status -> ". $response->getBody());
                if ($res->responseCode == "200") {
                    if (($dataPayment->datetime_expired < Carbon::now()->toDateTimeString()) && ($res->transactionStatus == 'PENDING')) {
                        $updateTransactions = $this->expiredTrx($dataPayment->client_transactions_id);
                        if ( !empty($updateTransactions) && $updateTransactions->isSuccess) {
                            $ottopayDb = $updateTransactions->data;
                            dispatch(new CheckUpdatePayment($ottopayDb));
                            echo "Transaction with payment_id ". $dataPayment->payment_id ." is Expired \n";
                        }
                    } else {
                        $updateTransactions = $this->checkPayStatus($res);
                        if ($updateTransactions->isSuccess) {
                            $ottoPay = $updateTransactions->data;
                            dispatch(new CheckUpdatePayment($ottoPay));
                        }
                    }
                } elseif ($res->responseCode == "407")  {
                    echo "Transaction with payment_id ". $res->responseDescription ."\n";
                } else {
                    echo "No Transactions \n";
                }
            });
            $promise->wait();

        }
    }


    private function checkPayStatus($resApi)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->data = null;
        
        $clientDb = ClientTransaction::with('OttoPay')
                        ->where('payment_id', $resApi->orderId)
                        ->first();

        if ( strtoupper( $clientDb->ottoPay->status_payment ) != strtoupper ( $resApi->transactionStatus) ) {

            $datePay = str_replace(" ", "-", substr($resApi->transactionTime, 0, 10));
            $timePay = substr($resApi->transactionTime, -8);
            $dateTimePay = date('Y-m-d H:i:s',strtotime( $datePay." ".$timePay ));

            $ottoPay = new CheckUpdateTemp();
            $ottoPay->transaction_time = $dateTimePay;
            $ottoPay->gross_amount = $resApi->grossAmount;
            $ottoPay->order_id = $resApi->orderId;
            $ottoPay->transaction_status = ucwords( strtolower( $resApi->transactionStatus ) );
            $ottoPay->param_req = json_encode( $resApi );
            // $ottoPay->reference_number = $resApi->referenceNumber;
            $ottoPay->save();

            $response->data = $ottoPay;
            $response->isSuccess = true;
            return $response;
        }

        return $response;
    }

    private function expiredTrx($clientTrxId)
    {    
        $ottopayDb = OttopayTransactions::with('clientTransaction')
                        ->where('client_transactions_id', $clientTrxId)
                        ->first();

        $datePay = Carbon::now()->toDateTimeString();
        $chekUpdt = new CheckUpdateTemp();
        $chekUpdt->transaction_time = $datePay;
        $chekUpdt->gross_amount = $ottopayDb->transaction_amount;
        $chekUpdt->transaction_status = "EXPIRED";
        $chekUpdt->param_req = null;
        $chekUpdt->order_id = $ottopayDb->clientTransaction->payment_id;
        $chekUpdt->save();

        $response = new \stdClass();
        $response->data = $chekUpdt;
        $response->isSuccess = true;
        return $response;
        
    }
}
