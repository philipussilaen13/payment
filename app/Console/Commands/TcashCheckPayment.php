<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use GuzzleHttp\Client;
use Carbon\Carbon;
use App\Console\Commands\DB;
use App\Http\Models\apiV1\TcashTransactions;
use App\Http\Models\apiV1\ClientTransaction;
use App\Http\Models\apiV1\CheckUpdateTemp;
use App\Http\Models\apiV1\PaymentChannel;
use GuzzleHttp\Exception\GuzzleException;
use App\Jobs\TcashUpdatePay;

class TcashCheckPayment extends Command
{
    private $urlWebDirect   = null;
    private $terminalId     = null;
    private $sigKey         = null;
    private $pwd            = null;
    private $userKey        = null;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tcash:paymentstatus';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check Tcash Web Checkout Payment Status';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->urlWebDirect    = env('TCASH_CHECK_TRX', 'https://tcash.telkomsel.com:11080/tcash-api/api/check/customer/transaction');
        $this->terminalId      = env('TCASH_TERMINAL', 'testing_tcash_wco');
        $this->sigKey          = env('TCASH_SIGKEY', 'wcotestsign');
        $this->pwd             = env('TCASH_PWD', '@wcotest12');
        $this->userKey         = env('TCASH_USERKEY', 'wcotest1090');
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // $payChannel = PaymentChannel::where('code', 'like', 'TCASH-WEB')->first();
        $data = \DB::table('client_transactions')
                // ->select('tcash_transactions.ref_number')
                ->join('tcash_transactions', 'client_transactions.id', '=', 'tcash_transactions.client_transactions_id')
                ->where('tcash_transactions.status', 'pending')
                // ->where('tcash_transactions.payment_channel', $payChannel->id)
                ->where('tcash_transactions.date_expired', '<', Carbon::now()->toDateTimeString())
                ->orderBy('tcash_transactions.id', 'desc')
                ->get();   

        $client = new Client([
            'base_uri' => $this->urlWebDirect, // Base URI is used with relative requests
            'headers' => array(
                'Content-Type' => 'application/x-www-form-urlencoded'
            )
        ]);
        foreach ($data as $key => $tcash) {      

            if ($tcash->method_code === 'TCASH') {
                $updateTransactions = $this->expiredTrx($tcash->client_transactions_id);
                if ( !empty($updateTransactions) && $updateTransactions->isSuccess) {
                    $tcashDb = $updateTransactions->data;
                    dispatch(new TcashUpdatePay($tcashDb));
                    echo "Transaction with payment_id ". $tcash->payment_id ." is Expired \n";
                } 
            } else {
                $refNum = $tcash->ref_number;

                // Send an asynchronous request.
                $request = new \GuzzleHttp\Psr7\Request('POST', $this->urlWebDirect,[
                    'body' => [
                        'refNum' => $refNum,
                        'terminalId' => $this->terminalId,
                        'userKey' => $this->userKey,
                        'passKey' => $this->pwd,
                        'signKey' => $this->sigKey
                    ]
                ]);
               
                $promise = $client->sendAsync($request)->then(function ($response) use ($refNum, $tcash) {
                    $res = json_decode($response->getBody());  
                    if (!empty($res) && !empty($res->status)) {
                        if ((!empty($res->status == "SUCCESS_COMPLETED") || !empty($res->status == "FAILED"))) {
                            $updateTransactions = $this->checkPayStatus($res);
                            if ( !empty($updateTransactions) && $updateTransactions->isSuccess) {
                                $tcashDb = $updateTransactions->data;
                                dispatch(new TcashUpdatePay($tcashDb));
                                echo "Transaction with ref_number ". $res->refNum ." is Success \n";
                            }
                        }
                    } elseif (empty($res->status))  {
                        $updateTransactions = $this->expiredTrx($tcash->client_transactions_id);
                        if ( !empty($updateTransactions) && $updateTransactions->isSuccess) {
                            $tcashDb = $updateTransactions->data;
                            dispatch(new TcashUpdatePay($tcashDb));
                            echo "Transaction with ref_number ". $refNum ." is Expired \n";
                        } 
                    } else {
                        echo "Transactions Not Found \n";
                    }
                });
                $promise->wait();
            }
            
        }
    }

    private function checkPayStatus($res)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->data = null;

        $tcashDb = TcashTransactions::with('clientTransaction')
                        ->where('ref_number', $res->refNum)
                        ->first();

        if ( ($tcashDb->clientTransaction->status == 'CREATED') && ($tcashDb->status == 'pending')  ) {

            $datePay = Carbon::now()->toDateTimeString();
            $tcashDb = new CheckUpdateTemp();
            $tcashDb->transaction_time = $datePay;
            $tcashDb->gross_amount = $res->amount;
            $tcashDb->transaction_status = ucwords( strtolower( $res->status ) );
            $tcashDb->param_req = json_encode( $res );
            $tcashDb->order_id = $tcashDb->client_transactions_id;
            $tcashDb->save();

            $response->data = $tcashDb;
            $response->isSuccess = true;
            return $response;
        }

        return $response;
    }

    private function expiredTrx($clientTrxId)
    {    
        $tcashDb = TcashTransactions::with('clientTransaction')
                        ->where('client_transactions_id', $clientTrxId)
                        ->first();

        $datePay = Carbon::now()->toDateTimeString();
        $chekUpdt = new CheckUpdateTemp();
        $chekUpdt->transaction_time = $datePay;
        $chekUpdt->gross_amount = $tcashDb->transaction_amount;
        $chekUpdt->transaction_status = "EXPIRED";
        $chekUpdt->param_req = null;
        $chekUpdt->order_id = $tcashDb->client_transactions_id;
        $chekUpdt->save();

        $response = new \stdClass();
        $response->data = $chekUpdt;
        $response->isSuccess = true;
        return $response;
        
    }

}
