<?php

namespace App\Console\Commands;

use App\Http\Models\apiV1\ClientPushCallback;
use App\Http\Models\apiV1\ClientTransaction;
use App\Jobs\SendClientCallback;
use Illuminate\Console\Command;

class ClientRepushCallback extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'client:repushCallback';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Re Push Client Callback';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // get failed client push callback
        echo "Get Failed Client Push Callback where retry less than 3 times\n";
        $failedPush = ClientPushCallback::where('status','FAILED')
            ->where('retry','<=',5)
            ->get();

        foreach ($failedPush as $item){
            $clientTransactionId = $item->client_transactions_id;
            echo "Process Client Transactions Id $clientTransactionId\n";
            $clientTransactionDb = ClientTransaction::find($clientTransactionId);
            echo "Dispatch jobs\n";
            dispatch(new SendClientCallback($clientTransactionDb));
        }
        echo "End\n";
    }
}
