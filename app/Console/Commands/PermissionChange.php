<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class PermissionChange extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'permission:change';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Change Permission Storage Logs';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        exec('chmod 777 -R /home/developer/payment/storage/logs');
        exec('chmod 777 -R /home/developer/payment/storage/payment');
        exec('chmod 777 -R /home/developer/payment/public/img/payment/yap');
    }
}
