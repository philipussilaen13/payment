<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\Http\Models\apiV1\EMoneyCard;
use Illuminate\Http\File;
use Riwandy\Ssh\PopBoxSSH;

class UploadMandiriSettlement extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
	private $host;
	private $port;
	private $username;
	private $password;
	private $cloud_path = '';
	
	protected $signature = 'mandirisettlement:upload';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Upload File Transaksi Emoney';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
		$this->host = \Config::get('remote.production.host');
		$this->port = \Config::get('remote.production.port');
		$this->username = \Config::get('remote.production.username');
		$this->password = \Config::get('remote.production.password');
		$this->cloud_path = env('MANDIRI_SETTLEMENT_PATH');
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        echo PHP_EOL.'mandiri settlement'.PHP_EOL;
		//$gap = -12;//hour old one
		$gap = -24;//hour
		$etime = time();
		$stime = strtotime($gap.' hours', $etime);
		
        $endDate = date('Y-m-d H:i:s', $etime);
		$startDate = date("Y-m-d H:i:s", $stime);
		// $startDate = '2017-03-08 00:00:00'; tanggal awal live mandiri e-money
		
		// manual generate mandiri settlement
		$oprId = '0043';
		$shiftId = '0001';
		$seqId = '01';
		$createdDate = date('dmYHi');
		$submitDate = date('dmYHi');
		$signature = '4514';
		
		$terminals = EMoneyCard::select('reader_id')
						->where('created_at', '>=', $startDate)
						->where('created_at', '<=', $endDate)
						->where('reader_id', '<>', '01234567')
						->where('transaction_amount','<',1000001)
						->whereNull('is_upload')
						->groupBy('reader_id')
						->get();

        if($terminals != null) {
			foreach($terminals as $terminal) {
				$terminalId = $terminal->reader_id;
				
				if($terminalId == '') continue;
				
				echo PHP_EOL.'terminal id: '.$terminalId.PHP_EOL;
				
				$fileName = $oprId.$shiftId.$terminalId.$seqId.$createdDate.$submitDate.$signature;

				$sumSettle = EMoneyCard::where('created_at', '>=', $startDate)
									->where('created_at', '<=', $endDate)
									->where('reader_id', $terminalId)
									->whereNull('is_upload')
									->sum('transaction_amount');

				$settlements = EMoneyCard::where('created_at', '>=', $startDate)
									->where('created_at', '<=', $endDate)
									->where('reader_id', $terminalId)
									->whereNull('is_upload')
									->get();

				$header = 'PREPAID';
				$header .= str_pad((count($settlements) + 2), 8, '0', STR_PAD_LEFT);
				$header .= str_pad((int)$sumSettle, 12, '0', STR_PAD_LEFT);
				$header .= $shiftId;
				$header .= $oprId;
				$header .= date('dmY');
				$header .= chr(3)."\n";
				$trailer = $oprId;
				$trailer .= str_pad(count($settlements), 8, '0', STR_PAD_LEFT);

				if($settlements != null) {			
					$content = $header;
					
					$cntr = 1;
					foreach($settlements as $settlement) {
						
						$setcode = substr( $settlement->settle_code, 0, 98);
						$ctr = str_pad($cntr, 6, "0", STR_PAD_LEFT );
						$content .= $setcode.$ctr.chr(3)."\n";
						//$content .= $settlement->settle_code.chr(3)."\n";
					$cntr++;		
					}

					$content .= $trailer.chr(3);			
				}

                foreach ($settlements as $key => $value) {
                    $value->name_file = $fileName;
                    $value->is_upload = 1;
                    $value->save();
                }

				// UPLOAD DATA SETTLEMENT
				$SSH = new PopBoxSSH([
					'host' => $this->host,
					'port' => $this->port,
					'username' => $this->username,
					'password' => $this->password,
				]);
				
                Storage::put('mandiri_settlement/'.$fileName.'.txt', $content);
				$SSH->uploadFile(storage_path().'/app/mandiri_settlement/'.$fileName.'.txt', (string)$this->cloud_path.$fileName.'.txt');
				
				$content2 = "";
				$filename2 = $oprId.$shiftId.$terminalId.$seqId.$createdDate.$submitDate.$signature.'.ok';
				Storage::put('mandiri_settlement/'.$filename2, $content2);
				$SSH->uploadFile(storage_path().'/app/mandiri_settlement/'.$filename2, (string)$this->cloud_path.$filename2);				
            }
		}
    }
}
