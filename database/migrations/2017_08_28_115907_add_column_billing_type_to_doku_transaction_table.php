<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnBillingTypeToDokuTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('doku_transactions', function (Blueprint $table) {
            $table->string('billing_type',50)->default('fixed')->after('payment_channel');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('doku_transactions', function (Blueprint $table) {
            $table->dropColumn('billing_type');
        });
    }
}
