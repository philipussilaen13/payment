<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApiSessionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('api_sessions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('company_access_tokens_id');
            $table->string('session_id',255);
            $table->string('ip_address',45)->nullable();
            $table->integer('last_activity');
            $table->integer('expired');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('company_access_tokens_id')->references('id')->on('company_access_tokens');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('api_sessions');
    }
}
