<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTokenPrivileges extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('token_privileges', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_access_tokens_id',false,true);
            $table->integer('api_modules_id',false,true);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('company_access_tokens_id')->references('id')->on('company_access_tokens');
            $table->foreign('api_modules_id')->references('id')->on('api_modules');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('token_privileges');
    }
}
