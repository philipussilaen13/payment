<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnToBniYapsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bni_yaps', function (Blueprint $table) {
            $table->string('pay_type',128)->nullable()->after('qr_data');
            $table->string('debit_pan',256)->nullable()->after('approval_code');
            $table->string('consumer_pan_masking',256)->nullable()->after('consumer_pan_raw');
            $table->text('inquiry_result')->nullable()->after('expired_datetime');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bni_yaps', function (Blueprint $table) {
            $table->dropColumn('pay_type');
            $table->dropColumn('debit_pan');
            $table->dropColumn('consumer_pan_masking');
            $table->dropColumn('inquiry_result');
        });
    }
}
