<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExpiredTimeToBniYapsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bni_yaps', function (Blueprint $table) {
            $table->dateTime('expired_datetime')->after('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bni_yaps', function (Blueprint $table) {
            $table->dropColumn('expired_datetime');
        });
    }
}
