<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableClientPushCallback extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_push_callbacks', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('client_transactions_id')->nullable();
            $table->unsignedInteger('client_transaction_details_id')->nullable();
            $table->string('payment_type',100)->nullable();
            $table->string('status',100)->nullable();
            $table->string('url',255)->nullable();
            $table->dateTime('last_push')->nullable();
            $table->integer('retry')->nullable();
            $table->text('error_message')->nullable();
            $table->text('request')->nullable();
            $table->text('response')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_push_callbacks');
    }
}
