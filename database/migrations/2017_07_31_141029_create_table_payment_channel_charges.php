<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePaymentChannelCharges extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_channel_charges', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('payment_channels_id');
            $table->string('name',100);
            $table->string('type',45)->default('fixed');
            $table->float('value');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('payment_channels_id')->references('id')->on('payment_channels');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_channel_charges');
    }
}
