<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableClientTransactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('payment_channels_id');
            $table->string('payment_id',255);
            $table->string('transaction_id',255);
            $table->string('client_name',100)->nullable();
            $table->string('customer_name',255);
            $table->string('customer_email',100)->nullable();
            $table->string('customer_phone',20)->nullable();
            $table->string('description',255)->nullable();
            $table->bigInteger('total_amount');
            $table->string('status')->default('WAITING');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('payment_channels_id')->references('id')->on('payment_channels');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_transactions');
    }
}
