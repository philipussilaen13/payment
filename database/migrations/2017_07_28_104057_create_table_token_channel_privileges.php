<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTokenChannelPrivileges extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('token_channel_privileges', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('company_access_tokens_id');
            $table->unsignedInteger('payment_channels_id');
            $table->string('status',45)->default('CLOSED');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('company_access_tokens_id')->references('id')->on('company_access_tokens');
            $table->foreign('payment_channels_id')->references('id')->on('payment_channels');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('token_channel_privileges');
    }
}
