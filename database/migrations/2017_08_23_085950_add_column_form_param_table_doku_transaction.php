<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnFormParamTableDokuTransaction extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('doku_transactions', function (Blueprint $table) {
            $table->text('form_params')->nullable()->after('session_id');
            $table->dateTime('datetime_expired')->nullable()->after('virtual_number');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('doku_transactions', function (Blueprint $table) {
            $table->dropColumn('form_params');
            $table->dropColumn('datetime_expired');
        });
    }
}
